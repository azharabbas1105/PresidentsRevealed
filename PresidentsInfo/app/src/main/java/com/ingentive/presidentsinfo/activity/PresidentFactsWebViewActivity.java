package com.ingentive.presidentsinfo.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Environment;
import android.support.annotation.IdRes;
import android.support.design.widget.CoordinatorLayout;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Select;
import com.ingentive.presidentsinfo.R;
import com.ingentive.presidentsinfo.activeandroid.PresidentInfo;
import com.ingentive.presidentsinfo.activeandroid.SettingsModel;
import com.ingentive.presidentsinfo.activeandroid.StoryInfo;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnMenuTabClickListener;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PresidentFactsWebViewActivity extends Activity implements View.OnTouchListener {

    //private BottomBar mBottomBar;
    private TextView tvQuotation;
    private WebView tvPreFactsText;
    private ImageView ivSign, ivPf;
    private int presId = 0;
    private PresidentInfo presidentInfo;
    private int random;
    private boolean content = false;
    private String folder_main_images = "Presidents_Stories/Images";
    private SettingsModel settingsModel;
    private int textSize;
    private String randomize;
    private ScrollView parentScroll;
    private ScrollView childScroll;
    private LinearLayout layoutContents, layoutRandomize, layoutSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_president_facts_web_view);
        initializeViews();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
// 224,63

        getFontSize();
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            presId = extras.getInt("president_id");
            presidentInfo = new PresidentInfo();
            presidentInfo = new Select().from(PresidentInfo.class).where("president_id=?", presId).executeSingle();
            if (presidentInfo != null) {
                showPresident(presidentInfo);
            }
        }
    }

    private void initializeViews() {
        try {
            layoutContents = (LinearLayout) findViewById(R.id.layout_president_fact_contents);
            layoutRandomize = (LinearLayout) findViewById(R.id.layout_president_fact_randomize);
            layoutSettings = (LinearLayout) findViewById(R.id.layout_president_fact_settings);

            layoutContents.setOnTouchListener(this);
            layoutRandomize.setOnTouchListener(this);
            layoutSettings.setOnTouchListener(this);

            tvPreFactsText = (WebView) findViewById(R.id.tv_p_f_text);
            tvQuotation = (TextView) findViewById(R.id.tv_quotation);
            ivSign = (ImageView) findViewById(R.id.iv_sign);
            ivPf = (ImageView) findViewById(R.id.iv_p_f);
            parentScroll = (ScrollView) findViewById(R.id.parentScroll);
            childScroll = (ScrollView) findViewById(R.id.childScroll);
            parentScroll.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    Log.v("", "PARENT TOUCH");
                    childScroll.getParent().requestDisallowInterceptTouchEvent(false);
                    return false;
                }
            });
            childScroll.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    Log.v("", "CHILD TOUCH");
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    return false;
                }
            });
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getRandom(List<Integer> array) {
        int rnd = new Random().nextInt(array.size());
        random = array.get(rnd);
        if (random == presId) {
            getRandom(array);
        }
        return random;
    }

    private void getFontSize() {
        settingsModel = new SettingsModel();
        settingsModel = new Select().from(SettingsModel.class).executeSingle();
        if (settingsModel == null) {
            SettingsModel model = new SettingsModel();
            model.randomize = "on";
            model.fontSize = 18;
            model.save();
            textSize = 18;
            randomize = "on";
        } else {
            textSize = settingsModel.getFontSize();
            randomize = settingsModel.getRandomize();
        }
    }

    private void showPresident(PresidentInfo presInfo) {
        String mFilePath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + folder_main_images + File.separator + presInfo.getPresSignatureImageName();
        Bitmap bitmap_ = StringToBitMap(mFilePath);
        BitmapDrawable ob = new BitmapDrawable(getResources(), bitmap_);
        ivSign.setBackgroundDrawable(ob);
        mFilePath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + folder_main_images + File.separator + presInfo.getPresImageName();
        Bitmap bitmap = StringToBitMap(mFilePath);
        ob = new BitmapDrawable(getResources(), bitmap);
        ivPf.setBackgroundDrawable(ob);
        tvQuotation.setText(android.text.Html.fromHtml(presInfo.getPresQuotation()));

        String str = presInfo.getPresFact();
        str = "<font color='white'>" + str + "</font>";
        String pish = "<html><head><style type=\"text/css\">@font-face {font-family: MyFont;src: url(\"file:///android_asset/fonts/mrseavesot-roman.ttf\")}body {font-family: MyFont;}</style></head><body>";
        String pas = "</body></html>";
        String myHtmlString = pish + str + pas;
        tvPreFactsText.loadUrl("about:blank");
        tvPreFactsText.loadDataWithBaseURL(null, myHtmlString, "text/html", "UTF-8", null);
        tvPreFactsText.setBackgroundColor(Color.TRANSPARENT);
        final WebSettings webSettings = tvPreFactsText.getSettings();
        webSettings.setDefaultFontSize((int) textSize);
        tvQuotation.setTextSize(textSize);
    }

    public Bitmap StringToBitMap(String path) {
        try {
            Bitmap bitmap = BitmapFactory.decodeFile(path);
            return bitmap;
        } catch (Exception e) {
            e.getMessage();
            Log.d("", "" + e.getMessage());
            return null;
        }
    }

    @Override
    public void onRestart() {
        super.onRestart();
        getFontSize();
        if (presidentInfo != null) {
            showPresident(presidentInfo);
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId()) {
            case R.id.layout_president_fact_contents:
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        layoutContents.setBackgroundResource(R.drawable.bottom_button_selected);
                        break;
                    case MotionEvent.ACTION_UP:
                        layoutContents.setBackgroundResource(R.drawable.bottom_button_unselect);

                        Intent intent = new Intent(PresidentFactsWebViewActivity.this, MainActivity.class);
                        intent.putExtra("btn_clicked", "btn_presidents");
                        startActivity(intent);
                        finish();
                        break;
                }
                break;
            case R.id.layout_president_fact_randomize:
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        layoutRandomize.setBackgroundResource(R.drawable.bottom_button_selected);

                        break;
                    case MotionEvent.ACTION_UP:
                        layoutRandomize.setBackgroundResource(R.drawable.bottom_button_unselect);

                        if (randomize.equals("on")) {
                            int preId = 0;
                            List<Integer> arrayList = new ArrayList<Integer>();
                            List<PresidentInfo> presidentInfoList = new ArrayList<PresidentInfo>();
                            presidentInfoList = new Select().all().from(PresidentInfo.class).orderBy("president_id ASC").execute();
                            for (int i = 0; i < presidentInfoList.size(); i++) {
                                arrayList.add(presidentInfoList.get(i).getPresId());
                            }
                            if (arrayList.size() > 1) {
                                preId = getRandom(arrayList);
                            }
                            PresidentInfo president_info = new PresidentInfo();
                            president_info = new Select().from(PresidentInfo.class).where("president_id=?", preId).executeSingle();
                            if (president_info != null) {
                                presidentInfo = president_info;
                                presId = preId;
                                showPresident(president_info);
                            }
                        } else {
                            Toast.makeText(PresidentFactsWebViewActivity.this, "Please Turn On Randomization", Toast.LENGTH_LONG).show();
                        }
                        break;
                }
                break;
            case R.id.layout_president_fact_settings:
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        layoutSettings.setBackgroundResource(R.drawable.bottom_button_selected);

                        break;
                    case MotionEvent.ACTION_UP:
                        layoutSettings.setBackgroundResource(R.drawable.bottom_button_unselect);

                        Intent intent = new Intent(PresidentFactsWebViewActivity.this, SettingsActivity.class);
                        startActivity(intent);
                        break;
                }
                break;
        }
        return true;
    }
}

