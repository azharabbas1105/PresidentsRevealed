package com.ingentive.presidentsinfo.common;


public class Utilities {
	public static String serverUrl = "http://pr.carrollwhite.com/";
	public static String servicesUrl = "services/";
	public static String presidentInfoUrl = "president_info";
	public static String urlPresidentsList = "presidents_list";
	public static String urlStoriesList = "stories_list";
	public static String urlFirstStory = "first_story";
	public static String urlStoryInfo = "story_info";
	public static String urlStoryGallery = "story_gallery";


}
