package com.ingentive.presidentsinfo.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.ingentive.presidentsinfo.R;
import com.ingentive.presidentsinfo.activeandroid.SettingsModel;
import com.ingentive.presidentsinfo.activeandroid.StoriesList;
import com.ingentive.presidentsinfo.activeandroid.StoryInfo;
import com.ingentive.presidentsinfo.common.NetworkChangeReceiver;
import com.ingentive.presidentsinfo.common.ServiceHandler;
import com.ingentive.presidentsinfo.common.Utilities;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.LinkedList;
import java.util.List;

public class SettingsActivity extends Activity implements View.OnClickListener {

    private Button btnSmall, btnMedium, btnLarge, btnOn, btnOff, btnBackToStory;
    private ProgressDialog mProgressDialog;
    public  int textSize = 18;
    private String randomize;
    SettingsModel settingsModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        initialize();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    private void initialize() {
        mProgressDialog = new ProgressDialog(SettingsActivity.this);
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(false);

        btnSmall = (Button) findViewById(R.id.btn_small);
        btnMedium = (Button) findViewById(R.id.btn_medium);
        btnLarge = (Button) findViewById(R.id.btn_large);
        btnOn = (Button) findViewById(R.id.btn_on);
        btnOff = (Button) findViewById(R.id.btn_off);
        btnBackToStory = (Button) findViewById(R.id.btn_back_to_story);

        btnSmall.setOnClickListener(this);
        btnMedium.setOnClickListener(this);
        btnLarge.setOnClickListener(this);
        btnOn.setOnClickListener(this);
        btnOff.setOnClickListener(this);
        btnBackToStory.setOnClickListener(this);

        settingsModel=new SettingsModel();
        settingsModel=new Select().from(SettingsModel.class).executeSingle();
        if(settingsModel==null) {
            SettingsModel model = new SettingsModel();
            model.randomize = "on";
            model.fontSize = 18;
            model.save();
            textSize=18;
            randomize = "on";
        }else {
            textSize=settingsModel.getFontSize();
            randomize=settingsModel.getRandomize();
        }
        if (textSize == 14) {
            btnSmall.setSelected(true);
            btnMedium.setSelected(false);
            btnLarge.setSelected(false);
        } else if (textSize == 18) {
            btnSmall.setSelected(false);
            btnMedium.setSelected(true);
            btnLarge.setSelected(false);
        } else if (textSize == 22) {
            btnSmall.setSelected(false);
            btnMedium.setSelected(false);
            btnLarge.setSelected(true);
        }
        if (randomize.equals("on")) {
            btnOn.setSelected(true);
            btnOff.setSelected(false);
        } else {
            btnOn.setSelected(false);
            btnOff.setSelected(true);
        }
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_small:
                settingsModel=new SettingsModel();
                settingsModel=new Select().from(SettingsModel.class).executeSingle();
                if(settingsModel!=null){
                    settingsModel.fontSize=14;
                    settingsModel.save();
                }
                textSize = 14;
                btnSmall.setSelected(true);
                btnMedium.setSelected(false);
                btnLarge.setSelected(false);
                break;
            case R.id.btn_medium:
                settingsModel=new SettingsModel();
                settingsModel=new Select().from(SettingsModel.class).executeSingle();
                if(settingsModel!=null){
                    settingsModel.fontSize=18;
                    settingsModel.save();
                }
                textSize = 18;
                btnSmall.setSelected(false);
                btnMedium.setSelected(true);
                btnLarge.setSelected(false);

                break;
            case R.id.btn_large:
                settingsModel=new SettingsModel();
                settingsModel=new Select().from(SettingsModel.class).executeSingle();
                if(settingsModel!=null){
                    settingsModel.fontSize=22;
                    settingsModel.save();
                }
                textSize = 22;
                btnSmall.setSelected(false);
                btnMedium.setSelected(false);
                btnLarge.setSelected(true);
                break;
            case R.id.btn_on:
                settingsModel=new SettingsModel();
                settingsModel=new Select().from(SettingsModel.class).executeSingle();
                if(settingsModel!=null){
                    settingsModel.randomize="on";
                    settingsModel.save();
                }
                btnOn.setSelected(true);
                btnOff.setSelected(false);
                break;
            case R.id.btn_off:
                settingsModel=new SettingsModel();
                settingsModel=new Select().from(SettingsModel.class).executeSingle();
                if(settingsModel!=null){
                    settingsModel.randomize="off";
                    settingsModel.save();
                }
                btnOn.setSelected(false);
                btnOff.setSelected(true);
                break;
            case R.id.btn_back_to_story:
                finish();
                break;
        }
    }
}
