package com.ingentive.presidentsinfo.activeandroid;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by PC on 10-08-2016.
 */
@Table(name = "DownLoadImage")
public class DownLoadImage extends Model {

    @Column(name = "story_id")
    public int storyId;

    @Column(name = "image_id")
    public int imageId;

    @Column(name = "timestamp")
    public double timestamp;

    public int getStoryId() {
        return storyId;
    }

    public void setStoryId(int storyId) {
        this.storyId = storyId;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public double getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(double timestamp) {
        this.timestamp = timestamp;
    }

    public DownLoadImage(){
        super();
        this.storyId=0;

        this.imageId=0;
        this.timestamp=0;
    }
}
