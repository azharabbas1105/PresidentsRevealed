package com.ingentive.presidentsinfo.activeandroid;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by PC on 10-08-2016.
 */
@Table(name = "StoryGallery")
public class StoryGallery extends Model {

    @Column(name = "story_id")
    public int storyId;

    @Column(name = "image_id")
    public int imageId;;

    @Column(name = "image_title")
    public String imageTitle;

    public int getStoryId() {
        return storyId;
    }

    public void setStoryId(int storyId) {
        this.storyId = storyId;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public String getImageTitle() {
        return imageTitle;
    }

    public void setImageTitle(String imageTitle) {
        this.imageTitle = imageTitle;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public double getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(double timestamp) {
        this.timestamp = timestamp;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getImageOrder() {
        return imageOrder;
    }

    public void setImageOrder(int imageOrder) {
        this.imageOrder = imageOrder;
    }


    @Column(name = "image_url")
    public String imageUrl;

    @Column(name = "image_order")
    public int imageOrder;

    @Column(name = "timestamp")
    public double timestamp;


    public StoryGallery(){
        super();
        this.storyId=0;
        this.imageId=0;
        this.imageTitle="";
        this.imageUrl="";
        this.imageOrder=0;
        this.timestamp=0;
    }
}
