package com.ingentive.presidentsinfo.activity;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.WindowManager;
import android.widget.TextView;

import com.activeandroid.query.Select;
import com.ingentive.presidentsinfo.R;
import com.ingentive.presidentsinfo.activeandroid.SettingsModel;

public class IntroductionActivity extends Activity {

    private TextView tvHeading,tvText;
    int textSize ;
    private SettingsModel settingsModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_introduction);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        settingsModel = new SettingsModel();
        settingsModel = new Select().from(SettingsModel.class).executeSingle();
        if (settingsModel == null) {
            SettingsModel model = new SettingsModel();
            model.randomize = "on";
            model.fontSize = 18;
            model.save();
            textSize = 18;
        } else {
            textSize = settingsModel.getFontSize();
        }

        tvHeading = (TextView)findViewById(R.id.tv_heading);
        tvText = (TextView)findViewById(R.id.tv_text);

        tvHeading.setText(Html.fromHtml("<h1>Why should anyone care about Millard Fillmore or Gerald Ford?</h1>"));
        tvText.setText(Html.fromHtml("<p>There are myriad reasons to care, but one of the most compelling is that the President of the United States, while in office, is our nation’s (and the world’s) foremost guardian of liberty – a liberty for which millions of Americans have been willing to fight and die to preserve.  As such, the presidents, all the president’s, wield enormous power, a power, which can precipitate and even control events well beyond his time in office. Every president’s actions stretch far into the future - our future.</p>\n" +
                "                <p>As president, Millard Fillmore set in motion a series of events, which long after he was gone, wrote for us one of the most horrifying chapters in the jam-packed history book known as the 20th Century. President Ford was thrust into the turning pages of that book, and wrote for us a heroic chapter of his own. Fillmore and Ford were thus inexplicably bound to one another and we are too.</p>\n" +
                "                <p>Presidents Revealed peeks into the souls of our nation’s highest leaders. The stories contained within acquaint us with times past but more remarkably they foretell the present.</p>\n" +
                "                <p>Some stories are heroic, some are tragic, and some dare to make us laugh. Common to them all however, is that they eagerly speak to us - both to the ennobling grandeur of our earthly potential, as well as the profound fragility that defines our humanity.</p>\n" +
                "                <p>We offer these little-known stories, written in a “who done it” style, as both tribute and heed. It is our fervent prayer that those who would be president, and equally important, those who would cast their ballots for him or her, should forever take to heart the solemn and timeless wisdom of those brave souls who “wrought a nation in our favor.”</p>"));
        tvText.setTextSize(textSize);
    }
}
