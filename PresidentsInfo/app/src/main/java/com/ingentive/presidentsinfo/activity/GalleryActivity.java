package com.ingentive.presidentsinfo.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.activeandroid.query.Select;
import com.ingentive.presidentsinfo.R;
import com.ingentive.presidentsinfo.activeandroid.DownLoadImage;
import com.ingentive.presidentsinfo.activeandroid.PresidentInfo;
import com.ingentive.presidentsinfo.activeandroid.StoryGallery;
import com.ingentive.presidentsinfo.activeandroid.StoryInfo;
import com.ingentive.presidentsinfo.common.NetworkChangeReceiver;
import com.ingentive.presidentsinfo.common.ServiceHandler;
import com.ingentive.presidentsinfo.common.Utilities;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class GalleryActivity extends Activity implements View.OnTouchListener {

    private static final String TAG = GalleryActivity.class.getSimpleName();
    private String folder_main = "Presidents_Stories";
    private String folder_main_images = "Presidents_Stories/Images";
    private String folder_story_gallery = "Presidents_Stories/Images/StoryGallery";
    private Button btnPrevious, btnClose, btnNext;
    private ImageView ivGallery;
    private ProgressDialog mProgressDialog;
    private int storyId;
    private int count = 1;
    //private int conn = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);

        try {
            File f = new File(Environment.getExternalStorageDirectory(), folder_main);
            if (!f.exists()) {
                f.mkdirs();
            }
            File imaages = new File(Environment.getExternalStorageDirectory(), folder_main_images);
            if (!imaages.exists()) {
                imaages.mkdirs();
            }
            File gallery = new File(Environment.getExternalStorageDirectory(), folder_story_gallery);
            if (!gallery.exists()) {
                gallery.mkdirs();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        btnPrevious = (Button) findViewById(R.id.btn_previous);
        btnClose = (Button) findViewById(R.id.btn_close);
        btnNext = (Button) findViewById(R.id.btn_next);
        ivGallery = (ImageView) findViewById(R.id.iv_gallery);

        btnPrevious.setOnTouchListener(this);
        btnClose.setOnTouchListener(this);
        btnNext.setOnTouchListener(this);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(false);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            storyId = extras.getInt("story_id");
            showpDialog();
            //conn = NetworkChangeReceiver.getConnectivityStatus(GalleryActivity.this);
            if (NetworkChangeReceiver.isConnected) {
                new getStoryGallery(storyId).execute();
            }else {
                hidepDialog();

                StoryGallery storyGallery = new StoryGallery();
                storyGallery = new Select().from(StoryGallery.class).where("story_id=? AND image_order=?", storyId, 1).executeSingle();
                if (storyGallery != null) {
                    try {
                        String mFilePath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + folder_story_gallery + File.separator +
                                storyGallery.getStoryId() + "_" + storyGallery.getImageId() + ".jpg";
                        Bitmap bitmap_ = StringToBitMap(mFilePath);
                        BitmapDrawable ob = new BitmapDrawable(getResources(), bitmap_);
                        ivGallery.setBackgroundDrawable(ob);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else {
                    Toast.makeText(GalleryActivity.this, "Please make sure, your network connection is ON ", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId()) {

            case R.id.btn_previous:
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        //layoutAudio.setBackgroundColor(Color.parseColor("#55624732"));
                        btnPrevious.setBackgroundResource(R.drawable.read_story_button_selected);
                        break;
                    case MotionEvent.ACTION_UP:
                        btnPrevious.setBackgroundResource(R.drawable.read_story_button_unselected);

                        if (count > 1) {
                            count = count - 1;
                            StoryGallery storyGallery = new Select().from(StoryGallery.class).where("story_id=? AND image_order=?", storyId, count).executeSingle();
                            if (storyGallery != null) {
                                try {
                                    String mFilePath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + folder_story_gallery + File.separator +
                                            storyGallery.getStoryId() + "_" + storyGallery.getImageId() + ".jpg";
                                    Bitmap bitmap_ = StringToBitMap(mFilePath);
                                    BitmapDrawable ob = new BitmapDrawable(getResources(), bitmap_);
                                    ivGallery.setBackgroundDrawable(ob);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } else {
                                count = count + 1;
                            }
                        }
                        break;
                }
                break;
            case R.id.btn_close:
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        //layoutAudio.setBackgroundColor(Color.parseColor("#55624732"));
                        btnClose.setBackgroundResource(R.drawable.read_story_button_selected);
                        break;
                    case MotionEvent.ACTION_UP:
                        btnClose.setBackgroundResource(R.drawable.read_story_button_unselected);
                        finish();
                        break;
                }
                break;
            case R.id.btn_next:
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        btnNext.setBackgroundResource(R.drawable.read_story_button_selected);

                        break;
                    case MotionEvent.ACTION_UP:
                        btnNext.setBackgroundResource(R.drawable.read_story_button_unselected);


                        showpDialog();
                        StoryGallery storyGallery = new StoryGallery();
                        count = count + 1;
                        storyGallery = new Select().from(StoryGallery.class).where("story_id=? AND image_order=?", storyId, count).executeSingle();
                        if (storyGallery != null) {
                            DownLoadImage downLoadImage = new DownLoadImage();
                            downLoadImage = new Select().from(DownLoadImage.class).where("story_id=? AND image_id=?",
                                    storyId, storyGallery.getImageId()).executeSingle();
                            if (downLoadImage == null) {
                                //conn = NetworkChangeReceiver.getConnectivityStatus(GalleryActivity.this);
                                if (NetworkChangeReceiver.isConnected) {
                                    new downloadImage(storyGallery).execute();
                                }else {
                                    hidepDialog();
                                    Toast.makeText(GalleryActivity.this, "Please make sure, your network connection is ON ", Toast.LENGTH_LONG).show();
                                }
                            } else if(downLoadImage.getTimestamp() < storyGallery.getTimestamp()){
                                //conn = NetworkChangeReceiver.getConnectivityStatus(GalleryActivity.this);
                                if (NetworkChangeReceiver.isConnected) {
                                    new downloadImage(storyGallery).execute();
                                }else {
                                    try {
                                        String mFilePath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + folder_story_gallery + File.separator +
                                                storyGallery.getStoryId() + "_" + storyGallery.getImageId() + ".jpg";
                                        Bitmap bitmap_ = StringToBitMap(mFilePath);
                                        BitmapDrawable ob = new BitmapDrawable(getResources(), bitmap_);
                                        ivGallery.setBackgroundDrawable(ob);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    hidepDialog();
                                }
                            }else {
                                try {
                                    String mFilePath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + folder_story_gallery + File.separator +
                                            storyGallery.getStoryId() + "_" + storyGallery.getImageId() + ".jpg";
                                    Bitmap bitmap_ = StringToBitMap(mFilePath);
                                    BitmapDrawable ob = new BitmapDrawable(getResources(), bitmap_);
                                    ivGallery.setBackgroundDrawable(ob);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                hidepDialog();
                            }
                        } else {
                            count = count - 1;
                            hidepDialog();
                        }
                        break;
                }
                break;
        }
        return true;
    }

    private void showpDialog() {
        if (!mProgressDialog.isShowing())
            mProgressDialog.show();
    }

    private void hidepDialog() {
        if (mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }

    private class getStoryGallery extends AsyncTask<Void, Void, Void> {
        int storyId = 0;
        StoryGallery galleryUpdate;

        public getStoryGallery(int storyId) {
            this.storyId = storyId;
            this.galleryUpdate = new StoryGallery();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showpDialog();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            // Creating service handler class instance

            List<NameValuePair> params = new LinkedList<NameValuePair>();
            params.add(new BasicNameValuePair("story_id", storyId + ""));

            ServiceHandler sh = new ServiceHandler();
            String jsonStr = sh.makeServiceCall(Utilities.serverUrl + Utilities.servicesUrl + Utilities.urlStoryGallery, ServiceHandler.POST, params);
            android.util.Log.d("Response: ", "> " + jsonStr);
            if (jsonStr != null) {
                try {
                    // {"STATUS":"NOT FOUND"}
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    JSONObject statusObj = jsonObj.getJSONObject("Success");
                    String code = statusObj.getString("code");
                    String content = statusObj.getString("content");
                    String error = jsonObj.getString("Error");

                    if (code.equals("200")) {
                        JSONArray data = jsonObj.getJSONArray("DATA");


                        for (int i = 0; i < data.length(); i++) {
                            JSONObject c = data.getJSONObject(i);

                            int sId = Integer.parseInt(c.getString("story_id"));
                            int image_id = Integer.parseInt(c.getString("image_id"));
                            String image_title = c.getString("image_title");
                            String image_url = c.getString("image_url");
                            int image_order = Integer.parseInt(c.getString("image_order"));
                            double timestamp = Double.parseDouble(c.getString("timestamp"));

                            StoryGallery storyGallery = new StoryGallery();
                            storyGallery = new Select().from(StoryGallery.class).where("story_id=? AND image_id=?", sId, image_id).executeSingle();
                            if (storyGallery == null) {
                                StoryGallery model = new StoryGallery();
                                model.storyId = sId;
                                model.imageId = image_id;
                                model.imageTitle = image_title;
                                model.imageUrl = image_url;
                                model.imageOrder = image_order;
                                model.timestamp = timestamp;
                                model.save();
                            }
                            if (storyGallery != null && storyGallery.timestamp < timestamp) {
                                storyGallery.storyId = sId;
                                storyGallery.imageId = image_id;
                                storyGallery.imageTitle = image_title;
                                storyGallery.imageUrl = image_url;
                                storyGallery.imageOrder = image_order;
                                storyGallery.timestamp = timestamp;
                                storyGallery.save();
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    hidepDialog();
                }
            } else {
                hidepDialog();
                android.util.Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            StoryGallery storyGallery = new StoryGallery();
            storyGallery = new Select().from(StoryGallery.class).where("story_id=? AND image_order=?", storyId, 1).executeSingle();
            if (storyGallery != null) {
                DownLoadImage downLoadImage = new DownLoadImage();
                downLoadImage = new Select().from(DownLoadImage.class).where("story_id=? AND image_id=?",
                        storyId, storyGallery.getImageId()).executeSingle();
                if (downLoadImage == null || downLoadImage.getTimestamp() < storyGallery.getTimestamp()) {
                    showpDialog();
                    new downloadImage(storyGallery).execute();
                } else {
                    try {
                        String mFilePath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + folder_story_gallery + File.separator +
                                storyGallery.getStoryId() + "_" + storyGallery.getImageId() + ".jpg";
                        Bitmap bitmap_ = StringToBitMap(mFilePath);
                        BitmapDrawable ob = new BitmapDrawable(getResources(), bitmap_);
                        ivGallery.setBackgroundDrawable(ob);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    hidepDialog();
                }
            }else {
                hidepDialog();
            }
        }
    }

    public Bitmap StringToBitMap(String path) {
        try {
            Bitmap bitmap = BitmapFactory.decodeFile(path);
            return bitmap;
        } catch (Exception e) {
            e.getMessage();
            Log.d("", "" + e.getMessage());
            return null;
        }
    }

    private class downloadImage extends AsyncTask<String, Integer, String> {
        StoryGallery gallery;

        public downloadImage(StoryGallery gallery) {
            this.gallery = gallery;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //showpDialog();
        }

        @Override
        protected String doInBackground(String... url) {

            int count;
            try {
                String imageName=gallery.getStoryId() + "_" + gallery.getImageId() + ".jpg";
                URL url_=new URL(gallery.getImageUrl());
                URLConnection conexion = url_.openConnection();
                conexion.connect();
                // downlod the file
                InputStream input = new BufferedInputStream(url_.openStream());
                OutputStream output = new FileOutputStream(Environment.getExternalStorageDirectory().getAbsolutePath()
                        + File.separator + folder_story_gallery + File.separator + imageName);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    output.write(data, 0, count);
                }

                output.flush();
                output.close();
                input.close();

                DownLoadImage downLoadImage = new DownLoadImage();
                downLoadImage = new Select().from(DownLoadImage.class).where("story_id=? AND image_id=?",
                        storyId, gallery.getImageId()).executeSingle();
                if (downLoadImage == null) {
                    DownLoadImage downLoadImage1 = new DownLoadImage();
                    downLoadImage1.storyId = storyId;
                    downLoadImage1.imageId = gallery.getImageId();
                    downLoadImage1.timestamp = gallery.getTimestamp();
                    downLoadImage1.save();
                } else {
                    downLoadImage.storyId = storyId;
                    downLoadImage.imageId = gallery.getImageId();
                    downLoadImage.timestamp = gallery.getTimestamp();
                    downLoadImage.save();
                }
                try {
                    final String mFilePath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + folder_story_gallery + File.separator +
                            imageName;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Bitmap bitmap_ = StringToBitMap(mFilePath);
                            BitmapDrawable ob = new BitmapDrawable(getResources(), bitmap_);
                            ivGallery.setBackgroundDrawable(ob);

                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
                hidepDialog();
            } catch (Exception e) {
                hidepDialog();
                Log.d(TAG, "Method downloadImages :  " + e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(String file_url) {
            hidepDialog();
        }
    }

    private void downloadImages(URL url_, String imageName, StoryGallery storyGallery) {
        int count;
        try {
            URLConnection conexion = url_.openConnection();
            conexion.connect();
            // downlod the file
            InputStream input = new BufferedInputStream(url_.openStream());
            OutputStream output = new FileOutputStream(Environment.getExternalStorageDirectory().getAbsolutePath()
                    + File.separator + folder_story_gallery + File.separator + imageName);

            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                output.write(data, 0, count);
            }

            output.flush();
            output.close();
            input.close();

            DownLoadImage downLoadImage = new DownLoadImage();
            downLoadImage = new Select().from(DownLoadImage.class).where("story_id=? AND image_id=?",
                    storyId, storyGallery.getImageId()).executeSingle();
            if (downLoadImage == null) {
                DownLoadImage downLoadImage1 = new DownLoadImage();
                downLoadImage1.storyId = storyId;
                downLoadImage1.imageId = storyGallery.getImageId();
                downLoadImage1.timestamp = storyGallery.getTimestamp();
                downLoadImage1.save();
            } else {
                downLoadImage.storyId = storyId;
                downLoadImage.imageId = storyGallery.getImageId();
                downLoadImage.timestamp = storyGallery.getTimestamp();
                downLoadImage.save();
            }
            try {
                final String mFilePath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + folder_story_gallery + File.separator +
                        imageName;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Bitmap bitmap_ = StringToBitMap(mFilePath);
                        BitmapDrawable ob = new BitmapDrawable(getResources(), bitmap_);
                        ivGallery.setBackgroundDrawable(ob);

                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
            hidepDialog();
        } catch (Exception e) {
            hidepDialog();
            Log.d(TAG, "Method downloadImages :  " + e.getMessage());
        }
    }
}
